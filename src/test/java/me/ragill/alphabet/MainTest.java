package me.ragill.alphabet;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void mainNoArguments() {
        try {
            Main.main();
            fail("exception expected");
        } catch (RuntimeException | URISyntaxException | IOException e) {
            assertNotNull(e);
        }
    }

    @Test
    void mainSampleFile() throws URISyntaxException, IOException {
        Main.main("fivebyfive.txt");
    }
}