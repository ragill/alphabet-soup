package me.ragill.alphabet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.AbstractList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NodeWalkerTest {

    private NodeWalker walker;
    private char[][] matrix;
    private int[] dimensions;
    private List<String> searchWords;
    private List<String> expected = new AbstractList<String>() {
        @Override
        public String get(int index) {
            return "tea 2:2 0:0";
        }

        @Override
        public int size() {
            return 1;
        }
    };
    @Mock
    MatrixParser parser;

    @BeforeEach
    public void setup() {
        this.dimensions = new int[]{3, 3};
        this.searchWords = new AbstractList<String>() {
            @Override
            public String get(int index) {
                return "tea";
            }

            @Override
            public int size() {
                return 1;
            }
        };
        this.matrix = new char[][]{
                {'A', 'B', 'C'},
                {'D', 'E', 'F'},
                {'G', 'H', 'T'}
        };
        when(parser.getDimensions()).thenReturn(this.dimensions);
        when(parser.getMatrix()).thenReturn(this.matrix);
        when(parser.getSearchWords()).thenReturn(this.searchWords);

    }


    @Test
    void search() {
        final NodeWalker walker = new NodeWalker(this.parser);
        final List<String> results = walker.search();
        assertNotNull(results);
        assertEquals(this.expected.size(), results.size());
        assertEquals(this.expected.get(0), results.get(0));
    }
}