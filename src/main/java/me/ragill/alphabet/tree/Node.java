package me.ragill.alphabet.tree;

public class Node {
    private char letter;
    private Location location;
    private Node child;
    private Compass orientation;


    public Node(char letter) {
        this.letter = letter;
    }

    public char getLetter() {
        return letter;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Node getChild() {
        return child;
    }

    public void setChild(Node child) {
        this.child = child;
    }

    public Compass getOrientation() {
        return orientation;
    }

    public void setOrientation(Compass orientation) {
        this.orientation = orientation;
    }
}
