package me.ragill.alphabet;

import me.ragill.alphabet.tree.Compass;
import me.ragill.alphabet.tree.Location;
import me.ragill.alphabet.tree.Node;

import java.util.*;

public class NodeWalker {
    private char[][] matrix;
    private int x;
    private int y;
    private List<String> searchWords;

    public NodeWalker(MatrixParser parser) {
        this.matrix = parser.getMatrix();
        final int[] dims = parser.getDimensions();
        this.x = dims[0];
        this.y = dims[1];
        this.searchWords = parser.getSearchWords();
    }

    public List<String> search() {
        final List<String> results = new ArrayList<>();
        //iterate over each word and return word along with beginning and ending location
        for (String searchWord : searchWords) {
            final StringBuilder builder = new StringBuilder(searchWord);
            //create stack to hold letters of word
            final Deque<Character> searchStack = this.createSearchStack(searchWord.toUpperCase(Locale.ROOT));
            //create tree structure for search word
            final Node tree = this.createTree(new Node(searchStack.remove()), searchStack);
            if (this.find(tree)) {
                builder.append(" ").append(tree.getLocation().toString()).append(" ").append(this.getLastLocation(tree));
                results.add(builder.toString());
            } else {
                builder.append(" - Not Found!");
                results.add(builder.toString());
            }
        }

        return results;
    }

    private boolean checkTree(Node tree) {
        if(tree == null){
            return true; //finshed checking
        }
        if(tree.getLocation() == null){
            return false;
        }
        return this.checkTree(tree.getChild());
    }

    private String getLastLocation(Node tree) {
        if (tree.getChild() == null) {
            return tree.getLocation().toString();
        }
        return this.getLastLocation(tree.getChild());
    }

    private boolean find(Node tree) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (tree.getLetter() == this.matrix[i][j] && traverse(tree.getChild(), i, j)) {
                    tree.setLocation(new Location(i, j));
                    if(this.checkTree(tree)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Search adjacent cells in matrix for match
     *
     * @param tree
     * @param i    - row in the matrix to search
     * @param j    - column in the matrix to search
     * @return true if found
     */
    private boolean traverse(Node tree, int i, int j) {
        if(tree.getOrientation() != null){
            return this.searchAdjacent(tree, i, j, tree.getOrientation());
        }
        for (Compass dir : Compass.values()) {
           this.searchAdjacent(tree,i,j,dir);
        }
        if (tree.getLocation() != null) {
            return true;
        }
        return false;
    }

    private boolean searchAdjacent(Node tree, int i, int j, Compass dir){
        final int adjacentX = i + dir.getX();
        final int adjacentY = j + dir.getY();
        //check matrix bounds
        if (adjacentX < 0 || adjacentX >= this.x || adjacentY < 0 || adjacentY >= this.y) {
            return false;
        }
        //if match found set location and then check the next node
        if (tree.getLetter() == this.matrix[adjacentX][adjacentY]) {
            tree.setLocation(new Location(adjacentX, adjacentY));
            if (tree.getChild() != null) {
                tree.getChild().setOrientation(dir);
                //added return statement to end search
               return this.traverse(tree.getChild(), adjacentX, adjacentY);
            }
        }
        return false;
    }

    private Deque<Character> createSearchStack(String searchWord) {
        final Deque<Character> searchStack = new ArrayDeque<>();
        for (char c : searchWord.toCharArray()) {
            searchStack.addLast(c);
        }
        return searchStack;
    }

    private Node createTree(Node node, Deque<Character> searchStack) {
        while (searchStack.size() > 0) {
            //create new node and add to parent
            final Node next = new Node(searchStack.remove());
            node.setChild(next);
            //recursively add nodes
            this.createTree(next, searchStack);
        }
        return node;
    }
}
