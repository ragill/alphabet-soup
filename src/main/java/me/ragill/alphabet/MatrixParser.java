package me.ragill.alphabet;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MatrixParser {
    private List<String> input;
    private int[] dimensions;
    private char[][] matrix;

    public MatrixParser(List<String> input) {
        this.input = input;
        this.parse();
    }

    private void parse() {
        //get the first line  - contains dimensions
        final String dims = this.input.remove(0);
        //parse the dimensions into an int array
        this.dimensions = Arrays.stream(dims.split("x")).mapToInt(s -> Integer.parseInt(s)).toArray();
        //extract the lines that contain the characters in the matrix
        final List<String> matrixBody = this.input.subList(0, dimensions[0]);
        //create the matrix
        this.matrix = matrixBody.stream().map(row -> row.toUpperCase(Locale.ROOT).replaceAll("\\s+", "").toCharArray()).toArray(char[][]::new);
        //remove the lines that contain the matrix bode leaving the search words
        input.removeAll(matrixBody);
    }

    public int[] getDimensions() {
        return dimensions;
    }

    public char[][] getMatrix() {
        return matrix;
    }

    public List<String> getSearchWords() {
        return input;
    }
}
