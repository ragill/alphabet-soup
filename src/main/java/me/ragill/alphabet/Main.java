package me.ragill.alphabet;

import jdk.jfr.events.FileReadEvent;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String... args) throws URISyntaxException, IOException {
        if (args.length == 0 || args[0].isEmpty()) {
            throw new RuntimeException("This application takes a path to a file as the sole input argument");
        }
        final Path path = Paths.get(args[0]);
        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
            final List<String> lines = reader.lines().collect(Collectors.toList());
            final MatrixParser parser = new MatrixParser(lines);
            final NodeWalker walker = new NodeWalker(parser);
            final List<String> results = walker.search();
            for (String result : results) {
                System.out.println(result);
            }

        }

    }

}
